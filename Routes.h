/*
 * Routes.h
 *
 *  Created on: 2014. 9. 6.
 *      Author: bae-unidev
 */

#ifndef ROUTES_H_
#define ROUTES_H_

#include "Request.h"
#include "Response.h"
#include "Controller.h"

class Routes {
public:
    Routes();
    void RouteUrl();
    const std::string setRoute();
private:
    Request request;
};


#endif /* ROUTES_H_ */
