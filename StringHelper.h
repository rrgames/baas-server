/*
 * StringHelper.h
 *
 *  Created on: 2014. 9. 7.
 *      Author: bae-unidev
 */

#ifndef STRINGHELPER_H_
#define STRINGHELPER_H_

#include <vector>
#include <string>
#include <openssl/sha.h>
#include <stdio.h>
class StringHelper {
public:
    static char HexToChar(char first, char second);
    static std::string TimeToString(time_t tm);
    static void Split(const std::string delimiter, std::string input, std::vector<std::string>* output);
static void SplitByToken(std::string delimiter, std::string input, std::vector<std::string>* output);
    static std::vector<std::string> Split(std::string delimiter, std::string str);
    static std::vector<std::string> SplitByToken(std::string delimiter, std::string str);
    static bool IsNumber(std::string& s);
    static std::string IntToString(int value);
	static std::string IntToString(long long int value);
    static std::string GetHash(std::string value);
    static std::string Trim(const std::string& str);
};

#endif /* STRINGHELPER_H_ */
