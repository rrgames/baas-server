//
//  StringFormat.h
//  JsonBuilder
//
//  Created by ARENA on 2014. 9. 22..
//  Copyright (c) 2014년 ARENA. All rights reserved.
//

#ifndef JsonBuilder_StringFormat_h
#define JsonBuilder_StringFormat_h

#include <stdarg.h>
#include <memory>
#include <string>
#include <stdlib.h>

std::string string_format(const std::string fmt_str, ...);

#endif
