//
//  JsonAtom.cpp
//  JsonBuilder
//
//  Created by ARENA on 2014. 9. 23..
//  Copyright (c) 2014년 ARENA. All rights reserved.
//

#include "JsonAtom.h"
#include "../StringHelper.h"

JsonAtom::JsonAtom(std::string str) {
    atom = str;
}

std::string JsonAtom::ToString() {
    std::string buf;
    buf += atom.c_str();
    return buf;
}

JsonNumber::JsonNumber(long long int numb) {
    atom = StringHelper::IntToString(numb);
}
JsonNumber::JsonNumber(int numb) {
    atom = StringHelper::IntToString(numb);
}

std::string JsonNumber::ToString() {
    return atom;
}

JsonBoolean::JsonBoolean(bool bl) {
    if(bl)
        atom = "true";
    else
        atom = "false";
}

std::string JsonBoolean::ToString() {

    return atom;
}
