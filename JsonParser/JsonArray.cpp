//
//  JsonArray.cpp
//  JsonBuilder
//
//  Created by ARENA on 2014. 9. 22..
//  Copyright (c) 2014년 ARENA. All rights reserved.
//

#include "JsonArray.h"
#include "JsonAtom.h"

JsonArray::~JsonArray() {
    for(int i=0; i<array.size(); i++) {
        delete array[i];
    }
//    printf("JSONARRAY DESTRUCTED.\n");
}

void JsonArray::Append(JsonObject * obj) {
    array.push_back(obj);
}

void JsonArray::Append(int number) {
    JsonNumber * objs = new JsonNumber(number);
    array.push_back(objs);
}
void JsonArray::Append(long long int number) {
    JsonNumber * objs = new JsonNumber(number);
    array.push_back(objs);
}

void JsonArray::Append(bool bl) {
    JsonBoolean * objs = new JsonBoolean(bl);
    array.push_back(objs);
}

void JsonArray::Append(std::string str) {
    JsonAtom * objs = new JsonAtom(str);
    array.push_back(objs);
}

std::string JsonArray::ToString() {

    std::string buffer;
    std::vector<JsonObject *>::iterator iter;

    buffer += "[";
    for(int i =0; i<array.size(); i++) {
        buffer += array[i]->ToString();
        buffer += ",";
    }
    buffer.erase(buffer.size()-1);
    buffer += "]";

    return buffer;
}
