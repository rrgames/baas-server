#ifndef _XMLSTAGE_PARSER_
#define _XMLSTAGE_PARSER_
#include <string>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <map>
class XMLStageParser {
private:
    xmlNode * root_elem;
    xmlDoc * doc;
    std::map<std::string, std::string> nodes;
public:
    XMLStageParser(std::string stage_name);
    ~XMLStageParser() {};
    std::string ParseStageXML(std::string elem_name);
private:
    void parseXMLRecur(xmlNode * node);
};

#endif
