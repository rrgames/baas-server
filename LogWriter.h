#ifndef __LogWriterModule__             
#define __LogWriterModule__ 

#include <string>
#include <time.h>
#include "Database/MongoDBPool.h"

class LogWriter {
public:
	std::string GetThisTime();
	void UserAdd(const std::string& user_key, const std::string& app_key);
	bool QueryAdd(const std::string& user_key, const std::string& app_key);
	void RankAdd(const std::string&  user_key, const std::string& app_key);
	static LogWriter* GetInstance();
	LogWriter();
private:
	static LogWriter * instance;


};
#endif
