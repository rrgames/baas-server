/*
 * StringHelper.cpp
 *
 *  Created on: 2014. 9. 7.
 *      Author: bae-unidev
 */

#include "StringHelper.h"
#include <sstream>


void StringHelper::Split(const std::string delimiter, std::string input, std::vector<std::string>* output) {

    int strlen = input.length();
    int dimlen = delimiter.length();
    if (dimlen == 0) {
	return;
    }
    int i = 0;
    int k = 0;

    while (i < strlen) {
        int j = 0;
        while (i + j < strlen && j < dimlen && input[i + j] == delimiter[j]) {
            j++;
        }

        if (j == dimlen) {
            output->push_back(input.substr(k, i - k));
            i += dimlen;
            k = i;
        } else {
            i++;
        }
    }

    output->push_back(input.substr(k, i - k));
}

std::vector<std::string> StringHelper::Split(std::string delimiter, std::string str) {
    std::vector<std::string> arr;

    int strlen = str.length();
    int dimlen = delimiter.length();
    if (dimlen == 0) {
        return arr;
    }

    int i = 0;
    int k = 0;

    while (i < strlen) {
        int j = 0;
        while (i + j < strlen && j < dimlen && str[i + j] == delimiter[j]) {
            j++;
        }

        if (j == dimlen) {
            arr.push_back(str.substr(k, i - k));
            i += dimlen;
            k = i;
        } else {
            i++;
        }
    }

    arr.push_back(str.substr(k, i - k));
    return arr;
}

void StringHelper::SplitByToken(std::string delimiter, std::string input, std::vector<std::string>* output) {

    std::string::size_type lastPos = input.find_first_not_of(delimiter, 0);
    std::string::size_type pos = input.find_first_of(delimiter, lastPos);

    while(std::string::npos != pos || std::string::npos != lastPos) {
        output->push_back(input.substr(lastPos, pos - lastPos));
        lastPos = input.find_first_not_of(delimiter, pos);
        pos = input.find_first_of(delimiter, lastPos);
    }
}
std::vector<std::string> StringHelper::SplitByToken(std::string delimiter, std::string str) {

    std::vector<std::string> arr;
    std::string::size_type lastPos = str.find_first_not_of(delimiter, 0);
    std::string::size_type pos = str.find_first_of(delimiter, lastPos);

    while(std::string::npos != pos || std::string::npos != lastPos) {
        arr.push_back(str.substr(lastPos, pos- lastPos));

        lastPos = str.find_first_not_of(delimiter, pos);

        pos = str.find_first_of(delimiter, lastPos);

    }
    return arr;
}

char StringHelper::HexToChar(char first, char second) {
    int digit;

    digit = (first >= 'A' ? ((first & 0xDF) - 'A') + 10 : (first - '0'));
    digit *= 16;
    digit += (second >= 'A' ? ((second & 0xDF) - 'A') + 10 : (second - '0'));
    return static_cast<char>(digit);
}
std::string StringHelper::IntToString(int value) {
    std::stringstream ss;
    ss << value;
    return ss.str();
}
std::string StringHelper::IntToString(long long int value) {
    std::stringstream ss;
    ss << value;
    return ss.str();
}
std::string StringHelper::TimeToString(time_t tm) {
    std::stringstream ss;
    ss << tm;
    return ss.str();
}
bool StringHelper::IsNumber(std::string& s) {
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}

std::string StringHelper::GetHash(std::string hash_string) {
    unsigned char hash[SHA_DIGEST_LENGTH];
    SHA1((unsigned char*)hash_string.c_str(), hash_string.size(), (unsigned char*)&hash);
    char mdString[SHA_DIGEST_LENGTH*2+1];
    for(int i=0; i<SHA_DIGEST_LENGTH; i++) {
        sprintf(&mdString[i*2], "%02x", (unsigned int)hash[i]);
    }
    std::string hashed = mdString;
    return hashed;
}
std::string StringHelper::Trim(const std::string& s)
{
   if(s.length() == 0)
       return s;
 
   std::size_t beg = s.find_first_not_of(" \a\b\f\n\r\t\v");
   std::size_t end = s.find_last_not_of(" \a\b\f\n\r\t\v");
   if(beg == std::string::npos) // No non-spaces
       return "";
 
   return std::string(s, beg, end - beg + 1);
}


