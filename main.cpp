#include <fcgi_stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include "mongo/client/dbclient.h"
#include "mongo/bson/bson.h"

#include "Environment.h"
#include "Routes.h"
#include "LogModule.h"

#include "Database/REDISPool.h"
#include "Database/MongoDBPool.h"


int main(void) {
mongo::client::initialize();
    MongoDBPool* mongoPool = MongoDBPool::GetInstance();
    RedisPool* redisPool = RedisPool::GetInstance();
    LogModule* logModule = LogModule::GetInstance();

    while(FCGI_Accept() >= 0) {
        Routes routes;
        routes.RouteUrl();
    }

    delete redisPool;
    delete mongoPool;
    delete logModule;

    return 0;
}
