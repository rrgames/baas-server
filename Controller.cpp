/*
 * Controller.cpp
 *
 *  Created on: 2014. 9. 7.
 *      Author: bae-unidev
 */

#include "Controller.h"
#include "Controllers/RanksController.h"
#include "Controllers/SessionController.h"
#include "Controllers/CollectionsController.h"
#include "Controllers/ApplicationController.h"

Controller* Controller::CreateController(const std::string& controllerName) {

    if(controllerName == "ranks")
        return (Controller*)new RanksController();
    else if(controllerName == "session")
        return (Controller*)new SessionController();
    else if(controllerName == "collections") 
    	return (Controller*)new CollectionsController();
    else if(controllerName == "application")
    	return (Controller*)new ApplicationController();

    else
        return NULL;
}

void Controller::RemoveController(Controller* pointer) {
    delete pointer;
}

void Controller::SetRequest(Request* request) {
    this->request = request;
}

Response* Controller::Action() {
    if (request == NULL)
        return NULL;

    switch(request->GetRestfulMethod()) {
    case Environment::Index:
        return this->ActionIndex();
        break;
    case Environment::Show:
        return this->ActionShow();
        break;
    case Environment::Create:
        return this->ActionCreate();
        break;
    case Environment::Update:
        return this->ActionUpdate();
        break;
    case Environment::Delete:
        return this->ActionDelete();
        break;
    case Environment::Unknown:
        return Response::CreateResponse(500, "{ message : 'not implement method" + this->request->GetMethodName() + "' }");
        break;
    }

    return Response::CreateResponse(500, "{ message : 'fucking restful method' }");
}

