/*
 * Request.cpp
 *
 *  Created on: 2014. 9. 7.
 *      Author: bae-unidev
 */

#include <stdlib.h>
#include <string.h>
#include "Request.h"
#include "Url.h"
#include "StringHelper.h"
#include "Controller.h"
#include <fcgi_stdio.h>

Request::Request() {
    std::string raw_url = Url::Decode(Environment::GetEnv(Environment::RequestUri));
    int q_loc = raw_url.find("?");
    std::string real_url;
    if(q_loc > 0) {
        real_url = raw_url.substr(0, q_loc);
    } else {
        real_url = raw_url;
    }

    if (Environment::GetEnv(Environment::Auth) != ""){
        std::string raw_token = Environment::GetEnv(Environment::Auth);

		int space_loc = raw_token.find(" ");
		if(space_loc > 0) {
			this->token = raw_token.substr(space_loc+1, raw_token.length());
		} else {
			this->token = raw_token;
		}
    }

    std::vector<std::string> paths;
    StringHelper::SplitByToken("/", real_url, &paths);

    int pathlen = paths.size();
    for (int i = 2; i < pathlen; i++) {
    	params["query[" + StringHelper::IntToString(i - 2) + "]"] = paths[i];
    }
    errorFlag = false;
    std::string value = Url::Decode(Environment::GetEnv(Environment::QueryString));
    if(Environment::GetHttpMethod() == Environment::POST ||
            Environment::GetHttpMethod() == Environment::PATCH)
        if (this->GetPostValue() == -1)
            errorFlag = true;

     if (value != "") {
        if (this->GenerateParameter(value) == -1) {
            errorFlag = true;
        }
    }
}


bool Request::HasError() {
    return this->errorFlag;
}

std::string Request::GetParam(const std::string& paramName) {
    if (params.find(paramName) != params.end()) {
        return params[paramName];
    }
    return "";
}
Environment::RestfulMethods Request::GetRestfulMethod() {
    return this->restMethod;
}

Environment::HttpMethods Request::GetHttpMethod() {
    if (Environment::GetEnv(Environment::RequestMethod) == "GET")
        return Environment::GET;
    else if (Environment::GetEnv(Environment::RequestMethod) == "POST")
        return Environment::POST;
    else if (Environment::GetEnv(Environment::RequestMethod) == "PATCH")
        return Environment::PATCH;
    else if (Environment::GetEnv(Environment::RequestMethod) == "DELETE")
        return Environment::DELETE;
}
std::string Request::GetAppKey() {
	return app_id;
}
std::string Request::GetUserKey() {
    return user_id;
}
std::string Request::GetToken() {
    return this->token;
}


unsigned long Request::GetContentLength() {
    this->contentLength = atoi(Environment::GetEnv(Environment::ContentLength).c_str());
    return this->contentLength;
}

std::string Request::GetMethodName() {
    // 아마 안쓰는 함수일듯.
    return this->methodName;
}
std::string Request::GetControllerName() {
    return this->controllerName;
}

int Request::GetPostValue() {
    // POST일때만 조건부동작함.
    char buffer[2048];
    int readed;
    memset(buffer, 0, 2048);
    readed = fread(buffer, 1, 2048, stdin);
    std::string buf;
    if(readed != -1) { // if read succeed.
        buf = Url::Decode(buffer);
        if(this->GenerateParameter(buf, true) == 0) {
            // succeed
            return 0;
        }
    } else {
        return -1;
    }
    return -1;
}

int Request::GenerateParameter(std::string param_str, bool post) {
    std::vector<std::string> pairs;
	StringHelper::Split("&", param_str, &pairs);
    std::vector<std::string> parts;
    int pairsize = pairs.size();
    if (pairsize <= 0)
	return -1; 
    for(int i = 0; i<pairsize; i++) {
        parts = StringHelper::Split("=", pairs[i]);
        if(parts.size() > 1 && parts[1] != "") {
            params[parts[0]] = Url::Decode(parts[1]);
        } else {
            // 이렇게 안찢어진 데이터가 있단 자체가 잘못된 데이터임.
            return -1;
        }
    }
    return 0;
}
