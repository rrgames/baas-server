#include "LogWriter.h"
LogWriter* LogWriter::instance = NULL;

LogWriter* LogWriter::GetInstance()
{
	if (!instance)
		instance = new LogWriter();
	return instance;
}	


LogWriter::LogWriter(){
}

std::string LogWriter::GetThisTime(){
	time_t timer;
	struct tm *t_now;
	timer = time(NULL); 
	t_now = localtime(&timer);
	char tmt[20];
	sprintf(tmt, "%04d-%02d-%02d %02d:%02d:%02d", 
	 t_now->tm_year + 1900,    
	 t_now->tm_mon + 1,
t_now->tm_mday, t_now->tm_hour, 0, 0);       
	std::string result = tmt;
	return result;
}
void LogWriter::UserAdd(const std::string& user_key, const std::string& app_key) {
	mongo::DBClientConnection* conn = MongoDBPool::GetInstance()->GetConnectionFromPool();   
	mongo::BSONObj p = conn->findOne("baas.users.log", MONGO_QUERY("app_key" << app_key << "user_key" << user_key << "Date" << GetThisTime()));
	if (!p.isEmpty()) {
		conn->update("baas.users.log", BSON("app_key" << app_key << "user_key" << user_key << "Date" << GetThisTime()), BSON("$inc" << BSON("user_count" << 1)));
	} else {
		conn->insert("baas.users.log", BSON("app_key" << app_key << "user_key" << user_key << "Date" << GetThisTime() << "user_count" << 1 << "query_count" << 0 << "rank_count" << 0));
	}
	MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
}

bool LogWriter::QueryAdd(const std::string& user_key, const std::string& app_key) {
	mongo::DBClientConnection* conn = MongoDBPool::GetInstance()->GetConnectionFromPool();   
	mongo::BSONObj p = conn->findAndModify("baas.users", BSON("app_key" << app_key << "user_key"<< user_key), BSON("$inc" << BSON("query_count" << 1)));
	if (!p.isEmpty()) {
	mongo::BSONObj pp = conn->findAndModify("baas.users.log", BSON("app_key" << app_key << "user_key" << user_key << "Date" << GetThisTime()), BSON("$inc" << BSON("query_count" << 1)));
		if (pp.isEmpty()) {
			conn->insert("baas.users.log", BSON("app_key" << app_key << "user_key" << user_key << "Date" << GetThisTime() << "user_count" << 0 << "query_count" << 1 << "rank_count" << 0));
		}
	} else {
		MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
		return false;
	}

	MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
	return true;
}

void LogWriter::RankAdd(const std::string& user_key, const std::string& app_key) {
	mongo::DBClientConnection* conn = MongoDBPool::GetInstance()->GetConnectionFromPool();   
	std::auto_ptr<mongo::DBClientCursor> cursor = conn->query("baas.users.log", MONGO_QUERY("app_key" << app_key << "user_key" << user_key << "Date" << GetThisTime()));
	if (cursor->more()) {
		conn->update("baas.users.log", BSON("app_key" << app_key << "user_key" << user_key << "Date" << GetThisTime()), BSON("$inc" << BSON("rank_count" << 1))); 
      	} else {
		conn->insert("baas.users.log", BSON("app_key" << app_key << "user_key" << user_key << "Date" << GetThisTime() << "user_count" << 0 << "query_count" << 0 << "rank_count" << 1));  
	}
	MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
}
