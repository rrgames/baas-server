/*
 * RanksController.h
 *
 *  Created on: 2014. 10. 18.
 *      Author: bae-unidev
 */

#ifndef CONTROLLERS_RANKSCONTROLLER_H_
#define CONTROLLERS_RANKSCONTROLLER_H_
#include <stdio.h>
#include "../Controller.h"

class RanksController : public Controller {

public:
	RanksController();
    const std::string GetControllerName();
    const std::string GetModelName();
    Response* Action();

private:
    Response* ActionIndexData();
    Response* ActionCreateData();
    Response* ActionShowScheme();
     Response* ActionIndex();
   Response* ActionShow();
    Response* ActionCreate();
    Response* ActionUpdate();
    Response* ActionDelete();
std::string GetRankData(std::string perfix, std::string user_id);
};


#endif /* CONTROLLERS_COLLECTIONSCONTROLLER_H_ */
