/*
 * CollectionsController.h
 *
 *  Created on: 2014. 10. 18.
 *      Author: bae-unidev
 */

#ifndef CONTROLLERS_COLLECTIONSCONTROLLER_H_
#define CONTROLLERS_COLLECTIONSCONTROLLER_H_
#include <stdio.h>
#include "../Controller.h"

class CollectionsController : public Controller {

public: 
    CollectionsController();
    virtual const std::string GetControllerName();
    virtual const std::string GetModelName();
    Response* Action();

private:
    Response* ActionIndexScheme();
    Response* ActionShowScheme();
    Response* ActionPatchScheme();
    Response* ActionCreateScheme();
    Response* ActionDeleteScheme();

    Response* ActionIndexData();
    Response* ActionShowData();
    Response* ActionCreateData();
    Response* ActionPatchData();
    Response* ActionDeleteData();

    virtual Response* ActionIndex();
    virtual Response* ActionShow();
    virtual Response* ActionCreate();
    virtual Response* ActionUpdate();
    virtual Response* ActionDelete();

    std::string collectionName;



 };


#endif /* CONTROLLERS_COLLECTIONSCONTROLLER_H_ */
