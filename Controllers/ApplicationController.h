/*
 * ApplicationController.h
 *
 *  Created on: 2014. 10. 28.
 *      Author: bae-unidev
 */

#ifndef CONTROLLERS_APPLICATIONCONTROLLER_H_
#define CONTROLLERS_APPLICATIONCONTROLLER_H_

#include <stdio.h>
#include "../Controller.h"

class ApplicationController : public Controller {

public:
	ApplicationController();
    virtual const std::string GetControllerName();
    virtual const std::string GetModelName();
    Response* Action();

private:
    virtual Response* ActionIndex();
    virtual Response* ActionShow();
    virtual Response* ActionCreate();
    virtual Response* ActionUpdate();
    virtual Response* ActionDelete();
    Response* ActionShowQuery();
Response* ActionShowLog();

};

#endif /* CONTROLLERS_APPLICATIONCONTROLLER_H_ */
