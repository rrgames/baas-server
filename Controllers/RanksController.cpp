/*
 * RanksController.cpp
 *
 *  Created on: 2014. 10. 18.
 *      Author: bae-unidev
 */

#include "RanksController.h"
#include "../Database/REDISPool.h"
#include "SessionContainer.h"

using namespace redis3m;

RanksController::RanksController() : Controller() {
    this->request = NULL;
}
const std::string RanksController::GetControllerName() {
    return "ranks";
}
const std::string RanksController::GetModelName() {
    return "rank";
}

Response* RanksController::Action() {
	Environment::HttpMethods httpMethod = this->request->GetHttpMethod();
	if (this->request->GetParam("query[1]") == "scheme")
		return this->ActionShowScheme();

	if (httpMethod == Environment::GET) {
		return this->ActionIndex();
	}else if (httpMethod == Environment::POST) {
		return this->ActionCreate();
	}

    return Response::ErrorHandler(105);
}

Response* RanksController::ActionShowScheme(){
	std::string result = "{ \"collectionName\" : \"ranks\", \"app_key\" : \"";
	result += request->GetAppKey() + "\", \"user_key\" : \"";
	result += request->GetUserKey() + "\", \"schemes\" : [";
	result += "{ \"name\" : \"user_id\"}, {\"name\" : \"score\"}, { \"name\" : \"channelName\"}, {\"name\" : \"channelValue\" } ] }";
	return Response::CreateResponse(200, result);
}

Response* RanksController::ActionIndex() {
	std::string n = this->request->GetParam("n");
	std::string q = this->request->GetParam("q");

	int r = 0;
	if (request->GetParam("r") != "")
		r = atoi(request->GetParam("r").c_str());
	if (r == 1)
		r = 0;

	std::string channel = "";// this->request->GetParam("cn") + ":" + this->request->GetParam("cv");
	std::string perfix = this->request->GetUserKey() + ":" + this->request->GetAppKey();// + ":";
	std::string result = "";

    	connection::ptr_t conn = RedisPool::GetInstance()->GetConnectionFromPool();
	
	if (this->request->GetParam("n") == "all") {
		if (!SessionContainer::GetInstance()->IsAdmin(request->GetToken())){
			RedisPool::GetInstance()->ReleaseConnectionToPool(conn);
			return Response::ErrorHandler(202);
		}
		std::vector<reply> keys = conn->run(command("SMEMBERS") << this->request->GetUserKey() + ":" + this->request->GetAppKey()).elements();
		std::string result = "{ \"results\" : [";
		if (keys.size() > 0) {
			for(std::vector<reply>::iterator iter = keys.begin(); iter != keys.end(); iter++) {
				std::vector<reply> ranks = conn->run(command("ZREVRANGE") << perfix + (*iter).str() << 0 << -1).elements();
				std::vector<std::string> record;
				StringHelper::Split(":", (*iter).str(), &record);
			for(std::vector<reply>::iterator iter2 = ranks.begin(); iter2 != ranks.end(); iter2++) {
	// UID가 루프돌면서 나옴. 그거가지고 스코어 찾으믄 됨.
				if(iter != keys.begin() || iter2 != ranks.begin())
					result += ", ";
				result += "{ \"channelName\" : \"" +  record[0] + "\", \"channelValue\" : \""+ record[1] + "\", \"score\" : " + conn->run(command("ZSCORE") << perfix + (*iter).str() << (*iter2).str()).str()  + ", \"user_id\" : \"" + (*iter2).str() + "\" }";  
				// uk:ak:cn:cv = user_ID sorted set
				// uk:ak:cn:cv:uid = data get
				// uk:ak = cn:cv set
			}			
}
		}
		result += "] }";
		RedisPool::GetInstance()->ReleaseConnectionToPool(conn);
		return Response::CreateResponse(200, result);
	}


	if (n == "one") {
		if (conn->run(command("EXISTS") << perfix + channel + ":" + q).integer() == 1) {

		int rank = conn->run(command("ZREVRANK") << perfix + channel << q).integer();
		if (r >= 2) {
			std::string range_result = "";
			std::vector<reply> range = conn->run(command("ZREVRANGE") << perfix + channel << rank - r/2 << rank + r/2).elements();
			if (range.size() > 0){
				for(std::vector<reply>::iterator iter = range.begin(); iter != range.end(); iter++) {
					if (iter != range.begin())
						range_result += ", ";
					range_result += GetRankData(perfix + channel, (*iter).str());		
	}
			}
			result += range_result;
		} else if (r == 0) {
	    std::string score = conn->run(command("ZSCORE") << perfix + channel << q).str();
	    std::string data = conn->run(command("GET") << perfix + channel + ":" + q).str();

	    result += "{ \"rank\" : " + StringHelper::IntToString(rank) + ",";
	    result += "\"score\" : " + score + ",";
		result += "\"user_id\" : \""+ q + "\", "; 
		if (data == "NULL") {
			result += "\"data\" : null }";
		}else{
			result += "\"data\" : \"" + data + "\"}";
		}

		}
		RedisPool::GetInstance()->ReleaseConnectionToPool(conn);
		return Response::CreateResponse(200, "{ \"results\" : [" + result + "] }");

	    // rank = ZRANK user_key:app_key:cn:cv q
		// score = ZSCORE user_key:app_key:cn:cv q
		// extra_data = GET user_key:app_key:cn:cv:q

		}else{
			RedisPool::GetInstance()->ReleaseConnectionToPool(conn);
			return Response::ErrorHandler(103);
		}
	}else if (n == "friend") {
		return Response::CreateResponse(400, "{ \"message\" : \"Not Implement method\" }");		
		
	}else if (n == "multi") {
		std::vector<std::string> uids;
		StringHelper::Split(",", q, &uids);
		result += "{ \"result\" : ["; 
		if (uids.size() > 0) {
			for(std::vector<std::string>::iterator iter = uids.begin(); iter != uids.end(); ++iter){
				std::string uid = StringHelper::Trim((*iter));
				const std::string data = GetRankData(perfix + channel, uid);				
				if (data != "" && iter != uids.begin())
					result += ",";
				result += data;
			}
		}
		result += "]}";
		RedisPool::GetInstance()->ReleaseConnectionToPool(conn);
		return Response::CreateResponse(200, result);
	} else if (n == "score") {
		std::string min = request->GetParam("min");
		std::string max = request->GetParam("max");
		std::vector<reply> range = conn->run(command("ZRANGEBYSCORE") << perfix + channel << min << max).elements();
		std::string range_result = "{ \"results\" : [";
		if(range.size() > 0)
		for(std::vector<reply>::iterator iter = range.begin(); iter != range.end(); iter++) { 
			if (iter != range.begin())
				range_result += ", ";
			range_result += GetRankData(perfix + channel, (*iter).str()); 
		}
		RedisPool::GetInstance()->ReleaseConnectionToPool(conn);                                              
		return Response::CreateResponse(200, range_result + "]}");
	}
	return Response::CreateResponse(500, "{ message : 'fucking restful method' }");
}

std::string RanksController::GetRankData(std::string perfix, std::string user_id)
{
	connection::ptr_t conn = RedisPool::GetInstance()->GetConnectionFromPool();
	std::string result = "";
	conn->run(command("MULTI"));
	conn->run(command("EXISTS") << perfix + ":" + user_id);
conn->run(command("ZREVRANK") << perfix << user_id);
	conn->run(command("ZSCORE") << perfix << user_id);
	conn->run(command("GET") << perfix + ":" + user_id);
	std::vector<reply> t = conn->run(command("EXEC")).elements();
	

	if (t[0].integer() == 1) {
		int rank = t[1].integer();
		std::string score = t[2].str();
		std::string data = t[3].str();
		result += "{ \"rank\" : " + StringHelper::IntToString(rank) + ",";
		result += "\"score\" : " + score + ",";
		result += "\"user_id\" : \"" + user_id + "\",";
		if (data == "NULL") {
			result += "\"data\" : null }";
		}else{
			result += "\"data\" : \"" + data + "\"}";
		}
	}
	RedisPool::GetInstance()->ReleaseConnectionToPool(conn);
	return result;
}

Response* RanksController::ActionCreate() {
	
	std::string uid = SessionContainer::GetInstance()->GetUserId(request->GetToken());	
	if (uid == "")
		return Response::ErrorHandler(202);

	std::string score = request->GetParam("score");
	if (score == "")
		return Response::ErrorHandler(104);

	std::string data = request->GetParam("data") == "" ? "NULL" : request->GetParam("data");
	std::string channelName = request->GetParam("cn") == "" ? "global" : request->GetParam("cn");
	std::string channel = channelName + ":" + request->GetParam("cv");

	std::string perfix = request->GetUserKey() + ":" + request->GetAppKey() + ":";
	connection::ptr_t conn = RedisPool::GetInstance()->GetConnectionFromPool();
	if (conn->run(command("EXISTS") << perfix + channel + ":" + uid).integer() == 1){
		// 이미 존재한다면...?
	} else {
		if (conn->run(command("SISMEMBER") << request->GetUserKey() + ":" + request->GetAppKey() << channel).integer() == 0)
		conn->run(command("SADD") << request->GetUserKey() + ":" + request->GetAppKey() << channel);
		conn->run(command("ZADD") << perfix + channel << score << uid);
		conn->run(command("SET") << perfix + channel + ":" + uid << data);
	}
	RedisPool::GetInstance()->ReleaseConnectionToPool(conn);
	
	return Response::CreateResponse(201, "{ \"message\" : \"create\" }");
}

Response* RanksController::ActionShow() {
    return Response::CreateResponse(200, "");
}

Response* RanksController::ActionUpdate() {
    return Response::CreateResponse(200, "");
}

Response* RanksController::ActionDelete() {
    return Response::CreateResponse(200, "");
}
