/*
 * CollectionsController.cpp
 *
 *  Created on: 2014. 10. 18.
 *      Author: bae-unidev
 */

#include "CollectionsController.h"
#include "SessionContainer.h"
#include "../Database/MongoDBPool.h"
#include "../LogModule.h"
#include "../Environment.h"
#include "../LogWriter.h"

using namespace mongo;

const std::string CollectionsController::GetControllerName() {
    return "collections";
}

const std::string CollectionsController::GetModelName() {
    return "collection";
}

CollectionsController::CollectionsController() : Controller() {

}

Response* CollectionsController::Action() {
	Environment::HttpMethods httpMethod = this->request->GetHttpMethod();
	if (this->request->GetParam("query[0]") == "collections") {
		return this->ActionIndexScheme();
	}
	collectionName = this->request->GetParam("query[0]");

	if (this->request->GetParam("query[1]") == "scheme") {
		if (httpMethod == Environment::GET) {
			return this->ActionShowScheme();
		} else if (httpMethod == Environment::PATCH){
			if (collectionName != "users" &&
					collectionName != "session" &&
					collectionName != "ranks")
			return this->ActionPatchScheme();
		} else if (httpMethod == Environment::POST){
			if (collectionName != "users" &&
					collectionName != "session" &&
					collectionName != "ranks")
			return this->ActionCreateScheme();
		} else if (httpMethod == Environment::DELETE){
			if (collectionName != "users" &&
					collectionName != "session" &&
					collectionName != "ranks")
			return this->ActionDeleteScheme();

		}
	}else if (this->request->GetParam("query[1]") == ""){
		if (httpMethod == Environment::GET)
			return this->ActionIndexData();
		else if (httpMethod == Environment::POST)
			return this->ActionCreateData();
	}else {
		if (httpMethod == Environment::GET) {
			return this->ActionShowData();
		} else if (httpMethod == Environment::PATCH){
			return this->ActionPatchData();
		}else if (httpMethod == Environment::DELETE){
			return this->ActionDeleteData();
		}
	}
    return Response::ErrorHandler(105);
}
// 최종 함수
Response* CollectionsController::ActionIndexScheme() {
	if (!SessionContainer::GetInstance()->IsAdmin(request->GetToken()))
		return Response::ErrorHandler(202);

	mongo::DBClientConnection* conn = MongoDBPool::GetInstance()->GetConnectionFromPool();
	std::string result = "{ \"collections\" : [";

 	mongo::BSONObjBuilder b;
	b << "collectionName" << 1 << "schemes" << 1 << "app_key" << 1 << "user_key" << 1 << "_id" << 0;
	const mongo::BSONObj bo = b.obj();

	std::auto_ptr<mongo::DBClientCursor> cursor = conn->query("baas.collections", MONGO_QUERY("app_key" << this->request->GetAppKey() << "user_key" << this->request->GetUserKey()), 0, 0, &bo, 0, 0);
	int index = 0;
	while (cursor->more()) {
		if (index != 0)
			result += ", ";

	    mongo::BSONObj p = cursor->next();
	    result += p.jsonString();
 	    index++;
	}
	result += "] }";
    MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
	return Response::CreateResponse(200, result);
}

Response* CollectionsController::ActionShowScheme() {
	if (!SessionContainer::GetInstance()->IsAdmin(request->GetToken()))
		return Response::ErrorHandler(202);

	DBClientConnection* conn = MongoDBPool::GetInstance()->GetConnectionFromPool();
	mongo::BSONObj p =  conn->findOne("baas.collections", MONGO_QUERY("collectionName" << collectionName << "app_key" << this->request->GetAppKey() << "user_key" << this->request->GetUserKey()));
	if(p.isEmpty()) {
	    MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
		return Response::ErrorHandler(102);
	}

	std::string result = "{ \"collectionName\" : \"" + collectionName +  "\",";
	result += "\"user_key\" : \"" + this->request->GetUserKey() + "\",";
	result += "\"app_key\" : \"" + this->request->GetAppKey() + "\",";
	result += p["schemes"].jsonString(mongo::JS) + "}";

	MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
	return Response::CreateResponse(200, result);
}

Response* CollectionsController::ActionCreateScheme() {
	if (!SessionContainer::GetInstance()->IsAdmin(request->GetToken()))
		return Response::ErrorHandler(202);

	mongo::DBClientConnection* conn = MongoDBPool::GetInstance()->GetConnectionFromPool();
	mongo::BSONObj p = conn->findOne("baas.collections", MONGO_QUERY("collectionName" << collectionName << "app_key" << this->request->GetAppKey() << "user_key" << request->GetUserKey()));
	if (!p.isEmpty())
	{
		MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
		return Response::CreateResponse(400, "{ \"message\" : \"이미 같은 이름의 컬렉션이 존재합니다.!\" }");
	}
	mongo::BSONObjBuilder b;
	b << "collectionName" << collectionName;
	b << "app_key" << request->GetAppKey() << "user_key" << request->GetUserKey();
	b.appendElements(mongo::fromjson(this->request->GetParam("insert_data")));

	conn->insert("baas.collections", b.obj());
	MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
	return Response::CreateResponse(201, "{ \"message\" : \"Success Create Collection!\" }");
}

// 최종함수
Response* CollectionsController::ActionIndexData() {
	LogModule::GetInstance()->Start();
	if (collectionName == "users" && SessionContainer::GetInstance()->GetUserId(request->GetToken()) == "")
		return Response::ErrorHandler(202);

	mongo::DBClientConnection* conn = MongoDBPool::GetInstance()->GetConnectionFromPool();

  	mongo::BSONObj collectionData = conn->findOne("baas.collections", MONGO_QUERY("collectionName" << collectionName << "app_key" << request->GetAppKey() << "user_key" << request->GetUserKey()));
	if(collectionData.isEmpty()) {
		MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
		return Response::ErrorHandler(102); // not found collection
	}

	std::string result = "{ \"collection_name\" : \"" + collectionName + "\", \"results\" : [ " ;
	std::string collectionObjId = collectionData["_id"].OID().toString();
	mongo::BSONObjBuilder b;
	if (this->request->GetParam("query") != ""){
		b.appendElements(mongo::fromjson(this->request->GetParam("query")));
	}
	int offset = this->request->GetParam("offset") == "" ? 0 : atoi(this->request->GetParam("offset").c_str());
	std::auto_ptr<mongo::DBClientCursor> cursor = conn->query("baas.collections.data_" + collectionObjId, b.obj(), 20, offset);
	int index = 0;
	while (cursor->more()) {
		if (index != 0)
			result += ", ";
		mongo::BSONObj p = cursor->next();
		result += "{ \"id\" : \"" + p["_id"].OID().toString() + "\"";
		for(mongo::BSONObj::iterator i = p.begin(); i.more(); ) {
			mongo::BSONElement e = i.next();
			if (e.type() != mongo::jstOID) {
				result += ", " + e.jsonString(mongo::JS);
			}
		}
		result += "}";
		index++;
	}
	result += "]}";
	MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
	LogModule::GetInstance()->Debug("", "INDEX DATA");
	return Response::CreateResponse(200, result);
}

// 최종 함수
Response* CollectionsController::ActionShowData() {
 	std::string objectId = this->request->GetParam("query[1]");

	mongo::DBClientConnection* conn = MongoDBPool::GetInstance()->GetConnectionFromPool();
	mongo::BSONObj collectionData = conn->findOne("baas.collections", BSON("collectionName" << collectionName << "app_key" << request->GetAppKey() << "user_key" << request->GetUserKey()));
	if (collectionData.isEmpty()) {
		MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
		return Response::ErrorHandler(102); // not found collection
	}

	std::string collectionObjId = collectionData["_id"].OID().toString();
	mongo::BSONObj obj;
	if (collectionName == "users" && objectId == "me")
	obj = conn->findOne("baas.collections.data_" + collectionObjId, BSON("_id" << mongo::OID(SessionContainer::GetInstance()->GetUserId(request->GetToken()) )));
	else
	obj = conn->findOne("baas.collections.data_" + collectionObjId, BSON("_id" << mongo::OID(objectId)));
	if (obj.isEmpty()) {
		MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
		return Response::ErrorHandler(103); // not found object
	}
	// std::string field = this->request->GetParam("field");
	std::string result = "{ \"result\" : ";
	obj = obj.removeField("_id");
	result += mongo::tojson(obj) + "}";
	MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
	return Response::CreateResponse(200, result);
}

// 최종 함수
Response* CollectionsController::ActionCreateData() {
	std::string insert_data = this->request->GetParam("insert_data");
	if (insert_data == "")
		return Response::ErrorHandler(104); // bad param.
	
	mongo::DBClientConnection* conn = MongoDBPool::GetInstance()->GetConnectionFromPool();
	mongo::BSONObj collectionData = conn->findOne("baas.collections", MONGO_QUERY("collectionName" << collectionName << "app_key" << request->GetAppKey() << "user_key" << request->GetUserKey()));
	if (collectionData.isEmpty()) {
		MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
		return Response::ErrorHandler(102); // not found collection
	}
	mongo::BSONObj obj = mongo::fromjson(insert_data);
	if (obj.hasField("id") || obj.hasField("_id")) {
		MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
		return Response::ErrorHandler(104); // bad param
	}

	std::string collectionObjId = collectionData["_id"].OID().toString();
	std::vector<mongo::BSONElement> column = collectionData.getField("schemes").Array();
	conn->insert("baas.collections.data_" + collectionObjId, obj);
	MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
	
	if (collectionName == "users")
		LogWriter::GetInstance()->UserAdd(request->GetUserKey(), request->GetAppKey());
	return Response::CreateResponse(201, obj.toString());
}

Response* CollectionsController::ActionDeleteData(){
	std::string objectId = request->GetParam("query[1]");

	mongo::DBClientConnection* conn = MongoDBPool::GetInstance()->GetConnectionFromPool();
	mongo::BSONObj collectionData = conn->findOne("baas.collections", MONGO_QUERY("collectionName" << collectionName << "app_key" << request->GetAppKey() << "user_key" << request->GetUserKey()));
	if (collectionData.isEmpty()) {
 		MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
		return Response::ErrorHandler(102); // not found collection
	}
	std::string collectionObjId = collectionData["_id"].OID().toString();
	conn->remove("baas.collections.data_" + collectionObjId, BSON("_id" << mongo::OID(objectId)));
	MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
return Response::CreateResponse(200, "{ \"message\" : \"success\" } "); 
}

// 최종함수
Response* CollectionsController::ActionDeleteScheme() {
	if (!SessionContainer::GetInstance()->IsAdmin(request->GetToken()))
		return Response::ErrorHandler(202); // not auth

	mongo::DBClientConnection* conn = MongoDBPool::GetInstance()->GetConnectionFromPool();
	mongo::BSONObj collectionData = conn->findOne("baas.collections", MONGO_QUERY("collectionName" << collectionName << "app_key" << request->GetAppKey() << "user_key" << request->GetUserKey()));
	if (collectionData.isEmpty()) {
	  	MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
		return Response::ErrorHandler(102); // not found collection
	}
	std::string collectionObjId = collectionData["_id"].OID().toString();;
	conn->remove("baas.collections", MONGO_QUERY("collectionName" << collectionName << "app_key" << request->GetAppKey() << "user_key" << request->GetUserKey()));
	conn->dropCollection("baas.collections.data_" + collectionObjId);
	MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
return Response::CreateResponse(200, "{ \"message\" : \"success\" } "); 
}


Response* CollectionsController::ActionPatchData() {
	std::string objectId = request->GetParam("query[1]");

 	mongo::DBClientConnection* conn = MongoDBPool::GetInstance()->GetConnectionFromPool();
	mongo::BSONObj collectionData = conn->findOne("baas.collections", MONGO_QUERY("collectionName" << collectionName << "app_key" << request->GetAppKey() << "user_key" << request->GetUserKey()));
	if (collectionData.isEmpty()) {
	  	MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
		return Response::ErrorHandler(102); // not found collection
	}
	std::string collectionObjId = collectionData["_id"].OID().toString();;
	conn->update("baas.collections.data_" + collectionObjId, MONGO_QUERY("_id" << mongo::OID(objectId)), BSON( "$set" << mongo::fromjson(request->GetParam("update_data"))));
	MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
	return Response::CreateResponse(200, "{ \"message\" : \"success\" } ");
}

Response* CollectionsController::ActionPatchScheme() {
 	mongo::DBClientConnection* conn = MongoDBPool::GetInstance()->GetConnectionFromPool();
	conn->update("baas.collections",
	MONGO_QUERY("collectionName" << collectionName << "app_key" << this->request->GetAppKey() << "user_key" << this->request->GetUserKey()),
	BSON( "$set" << mongo::fromjson(this->request->GetParam("update_data"))));
	MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
	return Response::CreateResponse(201, "success!");
}

Response* CollectionsController::ActionIndex() {
    return NULL;
}
Response* CollectionsController::ActionShow() {
    return NULL;
}
Response* CollectionsController::ActionUpdate() {
    return NULL;
}
Response* CollectionsController::ActionDelete() {
    return NULL;
}
Response* CollectionsController::ActionCreate() {
    return NULL;
}
