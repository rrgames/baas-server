/*
 * ApplicationController.cpp
 *
 *  Created on: 2014. 10. 28.
 *      Author: bae-unidev
 */

#include "ApplicationController.h"
#include "../Database/MongoDBPool.h"
#include "../LogModule.h"
#include "../Environment.h"

ApplicationController::ApplicationController(){

}
const std::string ApplicationController::GetControllerName(){
	return "application";
}
const std::string ApplicationController::GetModelName(){
	return "applications";
}

Response* ApplicationController::Action()
{
	if (this->request->GetParam("query[1]") == "log") {
		return this->ActionShowLog();
	}else if (this->request->GetParam("query[1]") == "query") {
		return this->ActionShowQuery();
	}
    return Response::ErrorHandler(105);
}
Response* ApplicationController::ActionShowLog() {
	mongo::DBClientConnection* conn = MongoDBPool::GetInstance()->GetConnectionFromPool();
	mongo::BSONObjBuilder b;
	b << "Date" << 1 << "user_count" << 1 << "query_count" << 1 << "rank_count" << 1 << "_id" << 0;
	const mongo::BSONObj bo = b.obj();         
	std::auto_ptr<mongo::DBClientCursor> cur = conn->query("baas.users.log", MONGO_QUERY("app_key" << request->GetAppKey() << "user_key" << request->GetUserKey()), 0, 0, &bo);
	std::string result = "{ \"result\" : [";	
	int i = 0;
	while(cur->more()){
		if (i != 0)
			result += ", ";
		result += mongo::tojson(cur->next());
		i++;
	}
	MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
	return Response::CreateResponse(200, result + "] }");
	
}


Response* ApplicationController::ActionShowQuery() {
	mongo::DBClientConnection* conn = MongoDBPool::GetInstance()->GetConnectionFromPool();
	std::auto_ptr<mongo::DBClientCursor> cur = conn->query("baas.users", MONGO_QUERY("app_key" << request->GetAppKey() << "user_key"<< request->GetUserKey()));
	if (cur->more()) {
		MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
		return Response::CreateResponse(200, "{ \"count\" : " + cur->next()["query_count"].toString(false, true) + " }");
	}
	MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
	return Response::CreateResponse(400, "FFFF");
}

Response* ApplicationController::ActionIndex(){
    return Response::CreateResponse(500, "{ messadsage : 'fucking restful method' }");
}

Response* ApplicationController::ActionShow(){
    return Response::CreateResponse(500, "{ messadsage : 'fucking restful method' }");
}

Response* ApplicationController::ActionCreate(){
    return Response::CreateResponse(500, "{ messadsage : 'fucking restful method' }");
}

Response* ApplicationController::ActionUpdate(){
    return Response::CreateResponse(500, "{ messadsage : 'fucking restful method' }");
}

Response* ApplicationController::ActionDelete(){
    return Response::CreateResponse(500, "{ messadsage : 'fucking restful method' }");
}
