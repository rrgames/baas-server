/*
 * SessionController.cpp
 *
 *  Created on: 2014. 9. 7.
 *      Author: into_arena
 */

#include "SessionController.h"
#include "../Database/MongoDBPool.h"
#include "SessionContainer.h"
#include "../OAuth/Kakao.h"
#include "../OAuth/Facebook.h"
#include "../LogModule.h"

const std::string SessionController::GetControllerName() {
    return "sessions";
}

const std::string SessionController::GetModelName() {
    return "session";
}

SessionController::SessionController() : Controller() {
    this->request = NULL;
}

Response* SessionController::Action() {
	if (request == NULL)
        	return NULL;

	Environment::HttpMethods httpMethod = this->request->GetHttpMethod();

	if (httpMethod == Environment::POST) {
        	return this->ActionCreate();
        } else if (httpMethod == Environment::DELETE) { 
		return this->ActionDelete();
   	}
    return Response::ErrorHandler(105);
}


Response* SessionController::ActionIndex() {
    return NULL;
}
Response* SessionController::ActionShow() {
    return NULL;
}
Response* SessionController::ActionUpdate() {
    return NULL;
}
Response* SessionController::ActionDelete() {
	SessionContainer::GetInstance()->RemoveToken( request->GetToken() );
    return NULL;
}
Response* SessionController::ActionCreate() {
	std::string type = request->GetParam("type");
	if (type == "password")
	{
		std::string id = request->GetParam("id");
		std::string password = request->GetParam("password");

	}else if (type == "kakao")
	{
		std::string token = request->GetParam("token");
		if (token == "")
			return Response::ErrorHandler(201);

		std::string kakao_uid = Kakao::GetMyId(token);
		std::string result = "";

		if (kakao_uid != "") {
			mongo::DBClientConnection* conn = MongoDBPool::GetInstance()->GetConnectionFromPool();
			mongo::BSONObj collectionData = conn->findOne("baas.collections", MONGO_QUERY("collectionName" << "users" << "app_key" << this->request->GetAppKey() << "user_key" << request->GetUserKey()));
			if (!collectionData.isEmpty())
			{
				std::string collectionObjId = collectionData["_id"].OID().toString();
				mongo::BSONObj obj = conn->findOne("baas.collections.data_" + collectionObjId, MONGO_QUERY("fb_uid" << kakao_uid));
				if (obj.isEmpty()) {
					mongo::BSONObj obj = mongo::BSONObjBuilder().genOID().append("full_name", Facebook::GetMyName(token)).append("kakao_uid", kakao_uid).append("username", kakao_uid).append("facebook_uid", "").append("email","").append("password", "").obj();
					conn->insert("baas.collections.data_" + collectionObjId, obj);
					result = SessionContainer::GetInstance()->CreateToken(obj["_id"].OID().toString());
				} else {
					result = SessionContainer::GetInstance()->CreateToken(obj["_id"].OID().toString());
				}
				MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
				return Response::CreateResponse(200, "{ \"session\" : \"" + result + "\"}");
			}
		} else {
			return Response::ErrorHandler(201);
		}
	} else if (type == "facebook") {
		std::string token = request->GetParam("token");
		if (token == "")
			return Response::ErrorHandler(201);

		std::string facebook_uid = Facebook::GetMyId(token);
		std::string result = "";

		if (facebook_uid != "") {
			// Users OID를 넣어야함.
			mongo::DBClientConnection* conn = MongoDBPool::GetInstance()->GetConnectionFromPool();
			mongo::BSONObj collectionData = conn->findOne("baas.collections", MONGO_QUERY("collectionName" << "users" << "app_key" << this->request->GetAppKey() << "user_key" << request->GetUserKey()));
			if (!collectionData.isEmpty())
			{
				std::string collectionObjId = collectionData["_id"].OID().toString();
				mongo::BSONObj obj = conn->findOne("baas.collections.data_" + collectionObjId, MONGO_QUERY("fb_uid" << facebook_uid));
				if (obj.isEmpty()) {
					mongo::BSONObj obj = mongo::BSONObjBuilder().genOID().append("full_name", Facebook::GetMyName(token)).append("fb_uid", facebook_uid).append("username", facebook_uid).append("kakao_uid", "").append("email","").append("password", "").obj();
					conn->insert("baas.collections.data_" + collectionObjId, obj);
					result = SessionContainer::GetInstance()->CreateToken(obj["_id"].OID().toString());
				} else {
					result = SessionContainer::GetInstance()->CreateToken(obj["_id"].OID().toString());
				}
				MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
				return Response::CreateResponse(200, "{ \"session\" : \"" + result + "\"}");
			}
		} else {
			return Response::ErrorHandler(201);
		}
	}
	return Response::ErrorHandler(104);
}

