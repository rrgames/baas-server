/*
 * SessionContainer.h
 *
 *  Created on: 2014. 10. 24.
 *      Author: bae-unidev
 */

#ifndef CONTROLLERS_SESSIONCONTAINER_H_
#define CONTROLLERS_SESSIONCONTAINER_H_
#include <string>

class SessionContainer {
public:
	SessionContainer();
	bool IsAdmin(const std::string& token);
	std::string GetUserId(const std::string& token);
	std::string CreateToken(const std::string& userId);
	void RemoveToken(const std::string& token);
	void AddAdminToken(const std::string& secret);
	std::string GetPermission(const std::string& token);

	static SessionContainer* GetInstance();
private:
	static SessionContainer* instance;

};

#endif /* CONTROLLERS_SESSIONCONTAINER_H_ */
