/*
 * SessionContainer.cpp
 *
 *  Created on: 2014. 10. 24.
 *      Author: bae-unidev
 */

#include "SessionContainer.h"
#include "../Database/REDISPool.h"
#include "../StringHelper.h"
using namespace redis3m;
SessionContainer* SessionContainer::instance = NULL;
SessionContainer* SessionContainer::GetInstance() {
    if (instance == NULL) {
        instance = new SessionContainer();
    }
    return instance;
}

SessionContainer::SessionContainer() {

}

void SessionContainer::AddAdminToken(const std::string& secret){
    connection::ptr_t conn = RedisPool::GetInstance()->GetConnectionFromPool();
    conn->run(command("SET") << "sessions:" + secret << "ADMIN");
   	RedisPool::GetInstance()->ReleaseConnectionToPool(conn);
 }


bool SessionContainer::IsAdmin(const std::string& token){
    connection::ptr_t conn = RedisPool::GetInstance()->GetConnectionFromPool();

		if (conn->run(command("GET") << "sessions:" + token).str() == "ADMIN"){
 		RedisPool::GetInstance()->ReleaseConnectionToPool(conn);
		return true;
}
 	else{
 		RedisPool::GetInstance()->ReleaseConnectionToPool(conn);
 		return false;
 	}
}

std::string SessionContainer::GetUserId(const std::string& token) {
    connection::ptr_t conn = RedisPool::GetInstance()->GetConnectionFromPool();
	if (conn->run(command("EXISTS") << "sessions:" + token).integer() == 1){
		std::string uid = conn->run(command("GET") << "sessions:" + token).str();
 		RedisPool::GetInstance()->ReleaseConnectionToPool(conn);
		return uid;
 	}else{
 		RedisPool::GetInstance()->ReleaseConnectionToPool(conn);
 		return "";
 	}
}

std::string SessionContainer::CreateToken(const std::string& userId) {
    connection::ptr_t conn = RedisPool::GetInstance()->GetConnectionFromPool();
    std::string token = StringHelper::GetHash(userId);
    if (conn->run(command("EXISTS") << "sessions:" + token).integer() == 0){
    	conn->run(command("SET") << "sessions:" + token << userId).str();
    	conn->run(command("EXPIRE") << "sessions:" + token << 648000);
    }
  	RedisPool::GetInstance()->ReleaseConnectionToPool(conn);
  	return token;
}

void SessionContainer::RemoveToken(const std::string& token){
    connection::ptr_t conn = RedisPool::GetInstance()->GetConnectionFromPool();
	conn->run(command("DEL") << "sessions:" + token);
  	RedisPool::GetInstance()->ReleaseConnectionToPool(conn);
}

std::string SessionContainer::GetPermission(const std::string& token){
	return "";
}
