/*
 * Response.h
 *
 *  Created on: 2014. 9. 6.
 *      Author: bae-unidev
 */

#ifndef RESPONSE_H_
#define RESPONSE_H_
#include <stdlib.h>
#include <string>

class Response {
public:
    static Response* CreateResponse(int code, const std::string& jsonData);


    static Response* ErrorHandler(int error_code);
    static void RemoveResponse(Response* pointer);
    void InitHeader(int code);
    void AddHeader(std::string header);
    void SetStatus(int code);
    void SetData(std::string jsonData);
    std::string GetResponse();
    void Render();

private:
    std::string status;
    std::string header;
    std::string body;

};

#endif /* RESPONSE_H_ */
