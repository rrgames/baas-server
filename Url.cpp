/*
 * Url.cpp
 *
 *  Created on: 2014. 9. 6.
 *      Author: bae-unidev
 */

#include "Url.h"

std::string Url::Encode(std::string input) {
    return "";
}

std::string Url::Decode(std::string input) {
    std::string result;
    int i;

    for (i = 0; i < input.length(); i++) {
        switch (input[i]) {
        case '+':
            result.append(1, ' ');
            break;
        case '%':
            if (i + 2 < input.length()
                    && std::isxdigit(input[i + 1])
                    && std::isxdigit(input[i + 2])
               ) {
                result.append(1, StringHelper::HexToChar(input[i + 1], input[i + 2]));
                i += 2;
            } else {
                result.append(1, '%');
            }
            break;
        default:
            result.append(1, input[i]);
            break;
        }
    }

    return result;
}
