#ifndef _FACEBOOK_H_
#define _FACEBOOK_H_


#include <curl/curl.h>
#include <string>
#include <vector>
class Facebook{
public:
  static std::string GetMyId(const std::string& token);
  static std::string GetMyName(const std::string& token);
  static std::string GetMyProfileImage(const std::string& token);
  static void GetMyFriendsList(const std::string& token, std::vector<std::string>* container);
};

#endif
