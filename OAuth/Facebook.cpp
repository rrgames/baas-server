
#include "Facebook.h"
#include "../RestClient.h"
#include <json-c/json.h>


std::string Facebook::GetMyName(const std::string& token){
  RestClient::clearAuth();
    std::string url = "https://graph.facebook.com/v2.1/me?fields=name&access_token="+ token;
  RestClient::response resp = RestClient::get(url);

    if(resp.code > 300 || resp.code < 199){
        return "";
    }
    json_object *jobj = json_tokener_parse(resp.body.c_str());

    std::string ret = "";
    if(json_object_object_get_ex(jobj, "name", &jobj)){
        ret = json_object_get_string(jobj);
    }
    return ret;
}

std::string Facebook::GetMyId(const std::string& token){
    RestClient::clearAuth();
    std::string url = "https://graph.facebook.com/v2.1/me?fields=id&access_token="+ token;
    RestClient::response resp = RestClient::get(url);

    if(resp.code >300 || resp.code < 199){
        return "";
    }
    json_object *jobj = json_tokener_parse(resp.body.c_str());

    std::string ret = "";
    if(json_object_object_get_ex(jobj, "id", &jobj)){
        ret = json_object_get_string(jobj);
    }
    return ret;
}

std::string Facebook::GetMyProfileImage(const std::string& token){
    RestClient::clearAuth();
    std::string url = "https://graph.facebook.com/v2.1/me/picture?redirect=false&access_token="+ token;
    RestClient::response resp = RestClient::get(url);

    if(resp.code >300 || resp.code < 199){
        return "";
    }
    json_object *jobj = json_tokener_parse(resp.body.c_str());
    std::string ret = "";
    if(json_object_object_get_ex(jobj, "data", &jobj)){
        if(json_object_object_get_ex(jobj, "url", &jobj))
            ret = json_object_get_string(jobj);

    }
    return ret;
}

void Facebook::GetMyFriendsList(const std::string& token, std::vector<std::string>* container){

    RestClient::clearAuth();
    std::string url = "https://graph.facebook.com/v2.1/me/friends?access_token="+ token;
    RestClient::response resp = RestClient::get(url);

    if(resp.code>300 || resp.code<199)
        return;

    json_object * jobj = json_tokener_parse(resp.body.c_str());
    json_object *jvalue;

    int json_length;

    std::string x;

    if(json_object_object_get_ex(jobj, "data", &jobj)){
        json_length = json_object_array_length(jobj);
        for(int i = 0; i<json_length; i++){
            jvalue=json_object_array_get_idx(jobj, i);
            if(json_object_object_get_ex(jvalue, "id", &jvalue)){
                container->push_back(json_object_get_string(jvalue));
            }
        }
    }

}
