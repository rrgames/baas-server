#include "Kakao.h"
#include "../RestClient.h"
#include <json-c/json.h>

std::string Kakao::GetMyId(const std::string& token){
    RestClient::clearAuth();
    std::string auth = " Bearer "+token;
    RestClient::setAuth("Authorization", auth);
    std::string url = "https://kapi.kakao.com/v1/user/me";
    RestClient::response resp = RestClient::get(url);


    if(resp.code >300 || resp.code < 199){
        return "";
    }
    json_object *jobj = json_tokener_parse(resp.body.c_str());

    std::string ret;
    if(json_object_object_get_ex(jobj, "id", &jobj)){
        ret = json_object_get_string(jobj);
    }
    return ret;


}
std::string Kakao::GetMyName(const std::string& token){
    RestClient::clearAuth();
    std::string auth = " Bearer "+token;
    RestClient::setAuth("Authorization", auth);
    std::string url = "https://kapi.kakao.com/v1/user/me";
    RestClient::response resp = RestClient::get(url);


    if(resp.code >300 || resp.code < 199){
        return "";
    }
    json_object *jobj = json_tokener_parse(resp.body.c_str());

    std::string ret;
    if(json_object_object_get_ex(jobj, "properties", &jobj)){
        if(json_object_object_get_ex(jobj, "nickname", &jobj))
            ret = json_object_get_string(jobj);

    }
    return ret;
    
}
std::string Kakao::GetMyProfileImage(const std::string& token){
    RestClient::clearAuth();
    std::string auth = " Bearer "+token;
    RestClient::setAuth("Authorization", auth);
    std::string url = "https://kapi.kakao.com/v1/user/me";
    RestClient::response resp = RestClient::get(url);


    if(resp.code >300 || resp.code < 199){
        return "";
    }
    json_object *jobj = json_tokener_parse(resp.body.c_str());

    std::string ret;
    if(json_object_object_get_ex(jobj, "properties", &jobj)){
        if(json_object_object_get_ex(jobj, "profile_image", &jobj))
            ret = json_object_get_string(jobj);

    }
    return ret;
}
