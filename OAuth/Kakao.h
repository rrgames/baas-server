#ifndef _KAKAO_H_
#define _KAKAO_H_

#include <string>
#include <curl/curl.h>

class Kakao{
public:
    static std::string GetMyId(const std::string& token);
    static std::string GetMyName(const std::string& token);
    static std::string GetMyProfileImage(const std::string& token);
};

#endif
