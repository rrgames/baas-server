/*
 * MysqlPool.h
 *
 *  Created on: 2014. 9. 17.
 *      Author: bae-unidev
 */

#ifndef MONGODBPOOL_H_
#define MONGODBPOOL_H_

#include <stdio.h>
#include <string>
#include <map>
#include "mongo/client/dbclient.h"
#include "mongo/bson/bson.h"

class MongoDBPool {
public:
    MongoDBPool(std::string db, std::string url);
    ~MongoDBPool();
    mongo::DBClientConnection* GetConnectionFromPool();
    void ReleaseConnectionToPool(mongo::DBClientConnection* database);
    bool CreatePool(int poolSize);
    static MongoDBPool* GetInstance();
    static MongoDBPool* instance;
    enum ConnectionStatus { USE, FREE, TEMP };

private:
    std::map<mongo::DBClientConnection*, MongoDBPool::ConnectionStatus> pool;
    int poolSize;
    int maxConnection;
    std::string databaseName;
    std::string url;
    mongo::DBClientConnection client;
    mongo::DBClientConnection* CreateConnection();
};

#endif /* MONGODBPOOL_H_ */
