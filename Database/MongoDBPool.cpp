/*
 * MongoDBPool.cpp
 *
 *  Created on: 2014. 9. 17.
 *      Author: bae-unidev
 */

#include "MongoDBPool.h"

MongoDBPool* MongoDBPool::instance = NULL;
MongoDBPool* MongoDBPool::GetInstance() {
    if (instance == NULL) {
        instance = new MongoDBPool("newdb_development", "localhost:27017");
        instance->CreatePool(4);
    }
    return instance;
}

MongoDBPool::MongoDBPool(std::string db, std::string url) {
    this->maxConnection = -1;
    this->poolSize = 0;
    this->databaseName = db;
    this->url = url;
 }

MongoDBPool::~MongoDBPool() {
    std::map<mongo::DBClientConnection*, MongoDBPool::ConnectionStatus>::iterator iter;
    for (iter = pool.begin(); iter != pool.end(); iter++) {
        delete iter->first;
    }
 }

mongo::DBClientConnection* MongoDBPool::CreateConnection()
{
    mongo::DBClientConnection* connection = NULL;
         connection = new mongo::DBClientConnection();
        connection->connect(this->url);
      return connection;

}

mongo::DBClientConnection * MongoDBPool::GetConnectionFromPool() {
     std::map<mongo::DBClientConnection*, MongoDBPool::ConnectionStatus>::iterator iter;
     mongo::DBClientConnection* connection = NULL;
    for (iter = pool.begin(); iter != pool.end(); iter++) {
        if (iter->second == MongoDBPool::FREE) {
        	connection = iter->first;
            iter->second = MongoDBPool::USE;
            break;
        }
    }
    if (connection == NULL) {
    	connection = this->CreateConnection();
        pool.insert(std::make_pair(connection, MongoDBPool::TEMP));
    }
    return connection;
}

void MongoDBPool::ReleaseConnectionToPool(mongo::DBClientConnection * connection) {
    std::map<mongo::DBClientConnection*, MongoDBPool::ConnectionStatus>::iterator iter = pool.find(connection);
    if(iter != pool.end()) {
        if(iter->second == MongoDBPool::USE)
            iter->second = MongoDBPool::FREE;

        if(iter->second == MongoDBPool::TEMP) {
        	delete iter->first;
            pool.erase(iter);
        }
    }
}

// 아예 처음부터 워커를 5개 만들어 놓는거였다니.
bool MongoDBPool::CreatePool(int poolSize) {
    for(int i = 0; i < poolSize; i++) {
    	mongo::DBClientConnection * connection = this->CreateConnection();
        if (connection != NULL) {
            pool.insert(std::make_pair(connection, MongoDBPool::FREE));
        }
    }

    if (poolSize == pool.size()) {
        this->poolSize = poolSize;
        return true;
    }
    return false;
}
