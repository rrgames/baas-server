/*
 * MysqlPool.cpp
 *
 *  Created on: 2014. 9. 17.
 *      Author: bae-unidev
 */

#include "REDISPool.h"

RedisPool* RedisPool::instance = NULL;
RedisPool* RedisPool::GetInstance() {
    if (instance == NULL)
        instance = new RedisPool("localhost");
    return instance;
}
RedisPool::RedisPool(std::string url) {
    this->url = url;
    this->pool = simple_pool::create(url);
}
RedisPool::~RedisPool() {
}
connection::ptr_t RedisPool::GetConnectionFromPool() {
    connection::ptr_t conn = this->pool->get();
    return conn;
}
void RedisPool::ReleaseConnectionToPool(connection::ptr_t pointer) {
    this->pool->put(pointer);
}
