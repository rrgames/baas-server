/*
 * MysqlPool.h
 *
 *  Created on: 2014. 9. 17.
 *      Author: bae-unidev
 */

#ifndef REDISPOOL_H_
#define REDISPOOL_H_

#include <redis3m/redis3m.hpp>
#include <string>
#include <map>

using namespace redis3m;

class RedisPool {
public:
    RedisPool(std::string url);
    ~RedisPool();
    connection::ptr_t GetConnectionFromPool();
    void ReleaseConnectionToPool(connection::ptr_t pointer);
    static RedisPool* GetInstance();
    static RedisPool* instance;

private:
    simple_pool::ptr_t pool;
    std::string url;
};

#endif /* REDISPOOL_H_ */
