/*
 * Routes.cpp
 *
 *  Created on: 2014. 9. 6.
 *      Author: bae-unidev
 */

#include "Routes.h"
#include <cstdlib>
#include <string.h>
#include "StringHelper.h"
#include "Controllers/SessionContainer.h"
#include "Database/MongoDBPool.h"
#include "LogWriter.h"
#include "LogModule.h"

Routes::Routes() {
	Request request;
}

void Routes::RouteUrl() {
	LogModule::GetInstance()->Start();
    if (request.HasError()) {
        Response* response = Response::ErrorHandler(104);
        response->Render();
        Response::RemoveResponse(response);
        return;
    }

    std::string controllerName = setRoute();
    if(controllerName == "") {
    	// app_id와 user_id가 일치하지 않음. 혹은 인자가 맞지 않
        Response* response = Response::ErrorHandler(104);
        response->Render();
        Response::RemoveResponse(response);
        return;
    }else if (controllerName == "createApp")
    {
        Response* response = Response::CreateResponse(200, "{ \"code\" : 200 }");
        response->Render();
        Response::RemoveResponse(response);
        return;
    }
    Controller * controller = Controller::CreateController(controllerName);
    controller->SetRequest(&request);
	LogModule::GetInstance()->Debug("", "Routeing");
    Response* response = controller->Action();
	LogModule::GetInstance()->Start();   
    response->Render();
    Response::RemoveResponse(response);

    Controller::RemoveController(controller);
	LogModule::GetInstance()->Debug("", "Rendering");     
}

const std::string Routes::setRoute() {

    std::vector<std::string> paths;
	StringHelper::SplitByToken("/?", Environment::GetEnv(Environment::RequestUri), &paths);

    int pathlen = paths.size();
    if(pathlen > 2) {
    	request.user_id = paths[0];
    	request.app_id = paths[1];

    	if(paths[2] == "createApp") {
			std::string secret = paths[3];
			mongo::DBClientConnection* conn = MongoDBPool::GetInstance()->GetConnectionFromPool();
			mongo::BSONObjBuilder b;
			b << "collectionName" << "users";
			b << "app_key" << request.GetAppKey() << "user_key" << request.GetUserKey();
			mongo::BSONArrayBuilder ab;
			ab.append(BSON("name" << "username"));
			ab.append(BSON("name" << "password"));
			ab.append(BSON("name" << "email"));
			ab.append(BSON("name" << "full_name"));
			ab.append(BSON("name" << "fb_uid"));
			ab.append(BSON("name" << "kakao_uid"));

			 b << "schemes" << ab.arr();
			conn->insert("baas.collections", b.obj());
			SessionContainer::GetInstance()->AddAdminToken(secret);
			mongo::BSONObjBuilder bb;
			bb << "app_key" << request.GetAppKey();
			bb << "user_key" << request.GetUserKey();
			bb << "query_count" << 1;
			conn->insert("baas.users", bb.obj());
			MongoDBPool::GetInstance()->ReleaseConnectionToPool(conn);
			return "createApp";
    	}


    	if (!LogWriter::GetInstance()->QueryAdd(request.user_id, request.app_id))
    		return "";

        if(paths[2] == "ranks") {
             return "ranks";
        } else if(paths[2] == "session") {
            return "session";
        } else if(paths[2] == "application") {
            return "application";
        } else {
        	return "collections";// path[3]
        }
    }
    return "";
}
