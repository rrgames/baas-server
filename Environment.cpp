/*
 * Environment.cpp
 *
 *  Created on: 2014. 9. 7.
 *      Author: bae-unidev
 */

#include "Environment.h"
#include <cstdlib>
#include <fcgi_stdio.h>

const char* Environment::DocumentRoot   = "DOCUMENT_ROOT";
const char* Environment::Auth 		= "HTTP_AUTHORIZATION";
const char* Environment::ContentLength  = "CONTENT_LENGTH";
const char* Environment::ContentType    = "CONTENT_TYPE";
const char* Environment::ScriptFilename = "SCRIPT_FILENAME";
const char* Environment::ScriptName     = "SCRIPT_NAME";
const char* Environment::ServerAdmin    = "SERVER_ADMIN";
const char* Environment::ServerName     = "SERVER_NAME";
const char* Environment::ServerPort     = "SERVER_PORT";
const char* Environment::ServerSoftware = "SERVER_SOFTWARE";
const char* Environment::HttpCookie     = "HTTP_COOKIE";
const char* Environment::HttpHost       = "HTTP_HOST";
const char* Environment::HttpReferer    = "HTTP_REFERER";
const char* Environment::HttpUserAgent  = "HTTP_USER_AGENT";
const char* Environment::Https          = "HTTPS";
const char* Environment::Path           = "PATH";
const char* Environment::QueryString    = "QUERY_STRING";
const char* Environment::RemoteAddr     = "REMOTE_ADDR";
const char* Environment::RemoteHost     = "REMOTE_HOST";
const char* Environment::RemotePort     = "REMOTE_PORT";
const char* Environment::RemoteUser     = "REMOTE_USER";
const char* Environment::RequestMethod  = "REQUEST_METHOD";
const char* Environment::RequestUri     = "REQUEST_URI";

Environment::HttpMethods Environment::GetHttpMethod() {
    if (Environment::GetEnv(Environment::RequestMethod) == "GET")
        return Environment::GET;
    else if (Environment::GetEnv(Environment::RequestMethod) == "POST")
        return Environment::POST;
    else if (Environment::GetEnv(Environment::RequestMethod) == "PATCH")
        return Environment::PATCH;
    else if (Environment::GetEnv(Environment::RequestMethod) == "DELETE")
        return Environment::DELETE;
}

std::string Environment::GetEnv(const char* envName) {

    if (std::getenv(envName) != NULL)
	return std::getenv(envName);
	return "";
}

std::string Environment::GetNowTime() {
    time_t now;
    struct tm * t_now;

	char tmt [20];
    time(&now);
    t_now = localtime(&now);

	sprintf(tmt, "%04d-%02d-%02d %02d:%02d:%02d",
			t_now->tm_year + 1900,
			t_now->tm_mon + 1,
			t_now->tm_mday,
			t_now->tm_hour,
			t_now->tm_min,
			t_now->tm_sec);
	std::string st = tmt;
    return st;
}
