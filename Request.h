/*
 * Request.h
 *
 *  Created on: 2014. 9. 6.
 *      Author: bae-unidev
 */

#ifndef REQUEST_H_
#define REQUEST_H_
#include <cstdlib>
#include <vector>
#include <map>
#include <string>
#include "Environment.h"

class Request {
public:
    Request();
    std::string GetParam(const std::string& paramName);
    Environment::HttpMethods GetHttpMethod();
    Environment::RestfulMethods GetRestfulMethod();
    unsigned long GetContentLength();
    bool HasError();
    std::string GetMethodName();
    std::string GetControllerName();
    void postMethodAnalyzer();
    int GetPostValue();
    int GenerateParameter(std::string str, bool post = false);
    std::string GetAppKey();
    std::string GetUserKey();
    std::string GetToken();



    std::string app_id;
    std::string user_id;
    std::string form_post_data;
    std::string token;

private:
    unsigned long contentLength;
    bool errorFlag;
    std::map<std::string, std::string> params;
    Environment::RestfulMethods restMethod;
    std::string controllerName;
    std::string methodName;
};


#endif /* REQUEST_H_ */
