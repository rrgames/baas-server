//
//  LogModule.h
//  JsonBuilder
//
//  Created by ARENA on 2014. 9. 23..
//  Copyright (c) 2014년 ARENA. All rights reserved.
//

#ifndef __JsonBuilder__LogModule__
#define __JsonBuilder__LogModule__

#include <string>
#include <time.h>
#include <fcgi_stdio.h>
#include <execinfo.h>
#include <unistd.h>
#include <stdlib.h>
#include <vector>

std::string backTracer();

class LogModule {
public:
    static LogModule* GetInstance() {
        if(!instance)
            instance = new LogModule();
        return instance;
    }

    LogModule();
    ~LogModule();
    void Debug(std::string method, std::string msg);
    void Info(std::string method, std::string msg);
    void Error(std::string method, std::string msg);
    void Url(std::string url, std::string msg);
    void Start();

private:
    static LogModule * instance;
    std::vector<clock_t> start_points;
    FILE* logFile;
    void logBuilder(std::string method, std::string type, std::string msg);
};

#endif /* defined(__JsonBuilder__LogModule__) */
