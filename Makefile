CC = gcc
TARGET = test
CXX = g++
SHELL = /bin/sh
ECHO  = /bin/echo

main.o : main.cpp 
	$(CXX) -g -c -o $@ main.cpp -pthread -lmongoclient -lboost_thread -lboost_system -lboost_regex -lboost_filesystem -I/usr/local/include/mongo -L/usr/local/lib

Database/MongoDBPool.o: Database/MongoDBPool.cpp
	$(CXX) -g -c -o $@ Database/MongoDBPool.cpp -pthread -lmongoclient -lboost_thread -lboost_system -lboost_regex -L/usr/local/lib

Request.o : Request.cpp
	$(CXX) -g -c -o $@ Request.cpp

LIBS =    main.o\
	  LogModule.o \
	   RestClient.o \
	  LogWriter.o \
           Database/MongoDBPool.o \
           Database/REDISPool.o \
	   OAuth/Kakao.o \
	   OAuth/Facebook.o \
           JsonParser/JsonArray.o \
           JsonParser/JsonObject.o \
           JsonParser/JsonAtom.o \
           Controllers/RanksController.o \
	   Controllers/SessionContainer.o \
	   Controllers/ApplicationController.o \
           Controllers/SessionController.o \
	   Controllers/CollectionsController.o \
           Controller.o \
           Environment.o \
           Request.o \
           Response.o \
           Routes.o \
           StringHelper.o \
           Url.o \

.PHONY : all
all : $(LIBS)
	$(CXX) -ggdb -g -o test.fcgi $(LIBS) -ljson-c -pthread -lmongoclient -lboost_thread -lboost_system -lboost_regex -lcurl -lboost_program_options $(shell pkg-config --cflags --libs redis3m) -lfcgi -lcrypto 

.PHONY : clean
clean :
	rm -rf *.exe *.o *~ *.bak
	rm Controllers/*.o
	rm Database/*.o
	rm JsonParser/*.o
	rm OAuth/*.o
	rm test.fcgi
